# wololo

**Github users : I’m leaving GitHub (because of Microsoft Acquisition).
Read [this](https://github.com/etnadji/301) for the new URL.
This repo will be ERASED soon.**

**Utilisateurs de Github : Je quitte GitHub (suite à son acquisition par Microsoft).
Lisez [ceci](https://github.com/etnadji/301) pour avoir la nouvelle URL.
Ce dépôt sera bientôt SUPPRIMÉ.**

## Requirements

- Python **2**.x
- *docopt*
- *mechanize*

### On Debian based distributions

```bash
sudo apt-get install python-docopt python-mechanize
```

### With PIP

```bash
pip2 install docopt
pip2 install mechanize
```

## Usage

### One link

```bash
wololo post [<url>] [--wurl=<WALLABAG_url>] [--login=<username>] [--password=<psswd>]
```

### Multiple links

```bash
wololo spost [<url_list>] [--wurl=<WALLABAG_url>] [--login=<username>] [--password=<psswd>]
```
